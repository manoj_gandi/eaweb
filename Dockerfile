FROM node:8.15.0

# Set production environment as default
ENV NODE_ENV ${NODE_ENV}
ENV HOST 0.0.0.0
ENV DOMAIN ${DOMAIN}
ENV COUNTRY ${COUNTRY}
ENV APP_ENV ${APP_ENV}

# Create app directory
RUN mkdir -p /home/EAweb

WORKDIR /home/EAweb


# Install packages
COPY package.json /home/EAweb/package.json
RUN npm install

# Make everything available for start
COPY . /home/EAweb
RUN npm run build

# Port for server

EXPOSE 3000
EXPOSE 8443

CMD ["npm", "start"]
